# 2021-02-08 XMPP Reivew notes



* Basics: Wiki https://en.wikipedia.org/wiki/XMPP
  * An applicationlayer protocol
  * Good for mid-power IoT devices
    * NOT power optimized, NOT memory optimized. Endpoint probably needs at least 1MB RAM, 1MB ROM, depending on the features implemented
    * Does implement many features that a big IoT deployment would need anyway:
      * Masive scalability
        * Multi-master, multi-slave,
        * Federation. Nodes connected to different servers can still communicate through interserver relay. Nodes do not need to know which server is the other node connected to.
      * Provision for security. Nodes do not need to know about each other, nor know any sensitive info e.g. IP addresses of their peers.
    * More expensive than MQTT, but also more features already-implemented that would be needed anyway
  * The format is extensible, which means that it is in a constant flux; there are many server implementations and dozens of client implementations;  expect them to be  NOT fully inter-operable, as they can choose not to implement functionality or have their own quirks
* Typical server, e.g. `ejabber` contains XMPP extensions that are important to know about, also servers of other services, e.g. MQTT; and these could become coupled on the server side by server-side scripting



## The RFCs

* RFC 6120 - XMPP Core :: https://datatracker.ietf.org/doc/html/rfc6120

  * The XML stream is a `stream` , with stream-level error conditions; provides encryption; it carries `Stanzas`
  * `Stanzas` are messages
  * there are 3 core message types: https://datatracker.ietf.org/doc/html/rfc6120#section-8.2 
    * `message`  = "push-mode" message -- like an email, has `from, to, message body`, the client decides recipient, does not expect response
      * the deployment endpoint would use this to report to the controller endpoint
      * The controller endpoint would use this to configure the deployment endpoint
    * `presence` = "broadcast mode" message,  or a 'publish-subscribe'-- used e.g. for presence announcement, the server decides recipient, client does not expect response. 
      * the deployment endpoint would use this for announcing that it's online. I imagine the deployment endpoint might want to stay offline for extended periods of time. 
      * This could be used for reduced-power reporting: the deployments could announce they have data using this mode, and all of their "friends" (controllers) would be notified. This might be better than message mechanism because all of the registered controllers would get notified 
      * THis i
    * `IQ` = "get/set" protocol iwith strict rules on error checking, and intended for receiving a response immediately, like with HTTP. Can be targetted at both client and server. 
      * This can be used for anything, but probably assumes that the nodes are online when the messages happen; in other words, the standarized message buffering and history is not used for these .

  * The 3 core message types are well defined, and enable higher level constructs and automation. The 3 types are probably well abstraced away in practical implementation libraries. 

    

* RFC6121 XMPP Instant messaging and presence :: https://datatracker.ietf.org/doc/html/rfc6121
  * [ READING IN PROGRESS ]





### Example usage scenario



For the STI-3087, I can image that:

	* The hub is an XMPP client, and a "resource" type node, of a given address
	* There are multiple redundant servers on the internet and the client can cycle between them
	* The XMPP Server is NOT A CONTROLLER, NOT A CUSTOMER. Instead, it merely facilitates connection and message buffering to a controller node. The controller node itself can be offline for extended periods of time, and rely on the XMPP server to buffer the messages.
	* There can be multiple controller nodes (multi-master) that will keep their `rooster` of endpoints to controll, using a mechanism akin to a "friends list"
 * the endpoint can have a single one or or multiple master nodes registered as 'friends' to listen to. THis is independent of being able to connect to multiple XMPP servers for redundancy





