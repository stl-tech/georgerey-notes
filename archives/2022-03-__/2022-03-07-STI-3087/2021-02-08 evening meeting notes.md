# 2021-02-08 evening meeting notes

* The top level requirements are set and signed

## New Tasks for me

* Look at the XMPP protocol
  * I am to become "the local expert"
* Prepare a docker image with an XMPP server

## Q

* Which XMPP server is our customer using?
	* Tigase, Ejabberd, Prosody, custom?
	* Do they have the controller/hub side of things ready?



## Resources



ejabber docker: https://hub.docker.com/r/ejabberd/ecs

Tigase docker: https://hub.docker.com/r/tigase/tigase-xmpp-server

Prosody docker: https://hub.docker.com/r/prosody/prosody/ ; https://github.com/prosody/prosody-docker 





