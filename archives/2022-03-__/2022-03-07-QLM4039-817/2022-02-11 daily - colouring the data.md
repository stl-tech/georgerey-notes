# 2022-02-11

## Meeting Nic : "how does our customer visualise data"



#### The signal path

Laser controller is connected to the FPGA

FPGA produces a spectrum

Spectrum enters the CPU space

Spectrum is converted to (intensity, density, range) {Q: What module does it?}

(intensity, density, range) goes to rasterizer

rasterizer produces 3x bitmap : (intensity, density, range) and invokes a callback at 

https://bitbucket.org/stl-tech/qlm_data/src/devel/