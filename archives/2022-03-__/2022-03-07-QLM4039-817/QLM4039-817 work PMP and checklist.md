 

# Scope

* [x] ✅Create experiment-app
* [x] ✅Create a dummy module and configure it with `json` using the `useComponent`
* [x] ✅In a component, create a thread and run it, and then stop it gracefully
* [x] ✅Between two components, send data somehow -- explore what is done in `TDLidar` so far on this point
* [x] ✅Build unit test to something that could potentially be testable
* [x] ✅Build the per-pixel blender
* [x] ✅Test the per-pixel blender
  * [x] ✅Result: Tests OK.
* [x] ✅Build the full-image blender
* [x] ✅Benchmark the full-image blender
  * [x] ✅Result: 30FPS on my devel machine. This is sufficient, no further optimization necessary.
  * [x] ✅Moreover, I suspect that in the actual application, we will not be shading **entire** image, but rather, a ROI
  * [x]  ✅**ToDo**  : Discuss with @Richard my experience of using the Polytec laser scanning vibrometer.
* [x] Write a nice documentation for the pixel blender
* [x] ✅prepare the Image blending demonstrator
* [x] ✅Write the user manual
  * [x] ❌Color maps tutorial: [Choosing Colormaps in Matplotlib — Matplotlib 3.5.1 documentation](https://matplotlib.org/stable/tutorials/colors/colormaps.html)
  * [x] ❌library of colormaps: [Third-party and user-contributed packages — Matplotlib third-party packages documentation](https://matplotlib.org/mpl-third-party/#colormaps-and-styles)
  * [x] ❗️2D colourmap system works, but might be not what we really want
  * [x] ❌ Maybe a "HSLA" colour blending system would work better
  * [x] ➡️Use the <code>L * a * b</code>  colour space for defining the key control points and blending.
* [x] 🌱Discuss the current colormap system with customer
  * [x] Grey for background
  * [x] Green for masks and zones
  * [x] Yellow to red for detection
  * [x] Blue for errors and a cursor
* [x] ❌Discuss the colormap UI with the UI developer -- we could use a separate card for colormap and scaling editing
* [x] ❌Discuss the global viewport mapping with the UI developer
  * [x] Zoom options -- zoom box/zoom cursor
* [x] Tend to use functional style =avoid side effects, to ease testing
* [x] ⬆️❗️Inject new API endpoint
* [x] ➡️Prepare the json configuration format
* [x] ⚠️Verify that configuration is updated via the API
* [ ] ⚠️Build a `behave` test or other test

# Anti-scope

Things that sound great but should not be done:

* Do not  move my env to the laptop. 
  * If I have no workspace, then it means that I cannot work, period.



# WBS



* [x] ✅ Review the current framework
* [ ] 🌱⬆️ Draw the current framework conceptual schematic
* [x] Build a small module configured using the `configuration`
* [x] Practice the modules, threads, and data passing methods
  * [x] `callback`
  * [x] `WorkQueue`
  * [x] `Topic`
* [x] Add `named callbacks` and `named threads` to make it easier to inspect and debug the live process
  * [x] Possibly inject a log-point into callback creation, that depends on a `#define` to enable for devel builds only
* [x] Inject a dummy image processing module, `IPM`
* [x] Write a nice pixel shader
* [x] Write a nice image shader
* [x] Write unit tests for pixel and image shader
* [x] Inject a dummy API endpoint
* [x] 🌱Develop the json configuration schema and have the image processing be configured with it
* [x] ⬆️❗️Serve something with the API endpoint
* [x] 🌱Write a test checking that something is being served from the API endpoint
* [x] ⚠️Receive a configuration update via an API endpoint
* [x] 🌱Process something with the `IPM` and serve it on the API endpoint
* [x] 🌱Receive a configuration update from the API endpoint
* [x] 🌱Review how does it look like from the UI side
* [x] 🌱⬆️Merge the changes to `Tools`
* [ ] 🌱Talk to the customer re demo and feedback
* [ ] 🌱Integrate `Sphinx` into `cmake` for automated docs generation
* [ ] 🌱Merge the changes to `devel`
* [ ] 🌱Factor out the `tools` into smaller namespaces, and estabilish a manual-writing plan



# Notes

https://stltech.atlassian.net/wiki/spaces/QLM4039/pages/398852099/Signal+Processing+Architecture







Initiating, Planning (scope, requirements, WBS, schedule, activities, costs, quality, resources, comms, risk, procurement, stakeholders)., Executing, Monitoring, Closing.





# Legend

❓ = hypothesis, for consideration

➡️ = possible action to try

✅ = finished, success, no need to revisit.

❌ = finished, did not help, no need to revisit. 

❗️- started, but no success yet due to some (likely) solvable problem on the way

🌱= ready to start

⚠️= not ready to start

⬆️ = in progress at the time of the last update. No result yet

📞 = decision needed



<i class="fa-solid fa-circle"></i>



