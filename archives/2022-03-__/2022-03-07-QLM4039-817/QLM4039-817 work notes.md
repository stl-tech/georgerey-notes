# Charter 

We need a new HTTP API that serves a combined (photon density, gas concentration image) ready-to-view image. 



photon density image – lidar generates a greyscale image of the background reflectors. Where the reflector is bright enough, gas concentration can be measured

gas concentration image – recovered gas of interest concentration. This is recovered using a spectral dip method.



# Assumptions

- There exist a service or two services that provide the source bitmaps
- There exist a configuration for the false colour map. The map already includes built-in log/antilog transformation, and thresholding as needed. 
- Colour mapping 
  - The colour mapping method A:
    - Split dimensions, output HSV 
      - 1x (1 in, 1 out) LUT for (photonDensity) -> V
      - 1x (1 in, 2 out) LUT for (gasConcentration) -> (H,S)
      - Combine HSV and convert to RGB
  - The colour mapping method B:
    - Joined dimensions, output RGB:
      - 1x (2 in, 3 out) LUT for (photonDensity, gasConcentration) -> (R,G,B) 
        - The LUT is a regular grid, nothing too fancy
        - We will have to check how much L2 Cache can we afford for this
        - This is for later.

- The mapping is a single-thread per output buffer; linear loop, per pixel: 
  - Load float photonDensity
  - Load float gasConcentration
  - Apply method A or method B
    - Ideally, the LUTs should fit into the core’s cache
  - Store RGB



### Notes

platform : Xilinx Zynq UltraScale+ XCZU4EV-1SFVC784E

Target one thread, one CPU core, no need for GPU here.

[Zynq UltraScale+ MPSoC Data Sheet: Overview (DS891) (xilinx.com)](https://www.xilinx.com/support/documentation/data_sheets/ds891-zynq-ultrascale-plus-overview.pdf)

Arch: 

- ARM-v8-A, A64 instruction set, 
- FPU (4SP/cycle) ; NEON SIMD, 
- caches: 
  - L1 = 32kB per core, 2-way SA;  2 ways, 2048 lines per way, 8-word (32byte) line; meaning the LUT data and pointers should be less than 32kB for best performance to account for locals and register spillover;  with 32byte strides; I should not worry too much about main mem data location as long as it is 8-word (32byte) aligned. 4xuint16 is 8 bytes; I get 4 pixels per cache line. That’s OK.
  - L2: 1MB shared, 16-way SA
- There are 32x Single or Double precision floating point registers (aliased, there is 32 total only) for the FPU. SIMD/Neon registers alias these registers but can use them as packed, vectors or matrix
- There are 30x general-purpose 64-bit registers; 32-bit values use low side of the 64-bit register (aliased)
- A+(B*C) is always fused, (== FMA)
- Hardware FP16<-> (FP32, FP64) conversion instruction; Hardware int64<->(FPx) instruction with configurable rounding
- 

2GB RAM DDR4, 32bit, 

Image size scale: 1280x720, x16bit, = 1800KB per plane, 

planes: input1, input2, output RGB → 5 planes → 9MB working memory

the input planes could be float32,, TODO: check the input type.

output to be uint16[1280x720x3] type 

Q: Will we want to add transparency (alpha) channel? This will be nice for overlaying all this over video preview image later on

CPU Docs: [Arm Cortex-A53 MPCore Processor Technical Reference Manual r0p4](https://developer.arm.com/documentation/ddi0500/j) 

[ARM Cortex-A Series Programmer’s Guide (cpen432.github.io)](https://cpen432.github.io/resources/arm-cortex-a-prog-guide-v4.pdf)

Instruction set incl. FPU instructions: [Microsoft Word - AArch64_ISA_interim_v30_0.doc](http://115.28.165.193/down/arm/arch/ARM_v8_Instruction_Set_Architecture_(Overview).pdf)

* The GPU supports OpenVG (2D vector Graphics) and OpenGL ES 2.0 (classic 3D graphics from before the compute shader era). 
* The GPU Does not support OpenCL. 
* The GPU's memory bandwidth is poor, and even if we use the OpenGL ES shaders, the best data type is "approximate FP16".
* In other words, this GPU might be useable for generating a HUD, and maybe driving a small on-device screen, but not useable for signal processing. We'll have to get NEON and FPGA to do that.





### Decisions

- Target the FPU and single-precision (32bit) first; if that is not sufficient, check for suitable NEON instructions. 



Update: Since we are only to serve 1 image per minute, there is no point trying to hand-optimise everything. Just focus on the basic plumbing, and the image processing method can be as slow as it needs to be (assuming that it is preemptable) 

---

George Rey, [21/01/2022 17:52]
# Legend

❓ = hypothesis, for consideration

➡️ = possible action to try

✅ = finished, success, no need to revisit.

❌ = finished, did not help, no need to revisit. 

❗️- started, but no success yet due to some (likely) solvable problem on the way

🟢 = ready to start

🟡 = not ready to start

⬆️ = in progress at the time of the last update. No result yet



