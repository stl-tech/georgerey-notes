# QLM4039-817. Use colour to inform 

## Introduction

The objective is to combine and compose the two sources of data : spatial distribution of light intensity, and spatial distribution of gas concentration. 

An example source of data may look like this 

	NOTE:This is not a final version of the preview images



Left: `background image.  This could be sourced from a visual preview camera, or from the light intensity channel of the TDLidar.

Right: "Gas concentration" sourced from the TDLidar output.


<table style="width:70%; margin:auto; align:centre; border: 1px solid black;">
<thead>
        <tr>
            <th colspan="3"><i class="fa-solid fa-user"></i></th>
        </tr>
</thead>
<tbody>
    <tr>
        <td>Visible light image</td>
        <td>TDLidar Gas concentration image</td>
        <td>Composite image</td>
	</tr>
    <tr>
        <td style="width:35%;"><img src="background-01.jpg"></img></td>
        <td style="width:35%;"><img src="density-01.png">   </img></td>
        <td style="width:35%;"><img src="result-01.png">    </img></td>
    </tr>
    <tr>
        <td style="width:35%;"><img src="background-02.jpg"></img></td>
        <td style="width:35%;"><img src="density-02.png">   </img></td>
        <td style="width:35%;"><img src="result-02.png">    </img></td>
    </tr>
    <tr>
        <td style="width:35%;"><img src="background-03.jpg"></img></td>
        <td style="width:35%;"><img src="density-03.png">   </img></td>
        <td style="width:35%;"><img src="result-03.png">    </img></td>
    </tr>
</tbody>
</table>




We want to combine these images in such way that the following constraints are met:

* The `background` image provides good orientation to the source of the `gas detection` 
* The colour mapping is perceptually uniform, informative, and not deceptive
* The colour mapping of gas concentration data is invertible, that is, it is possible to unambigously read the value of the concentration out of the image. A colour scale can be provided. A procedure for inversion can be provided.
* Whenever a conflict occurs between presenting the `background` image and the `gas detection` image, the `gas detection` has priority.
* Additionally, we would like the operator to be able to quickly visually distinguish between the following categories of pixels:
  * [a] The readout is valid, no gas detected
  * [b] The readout is valid, some gas detected ,but below the alarm threshold
  * [c] The readout is valid, `interesting` amounts of gas detected, but below the instrument saturation point
  * [d] There is gas detected, but the readout is saturated, hence, the exact value cannot be provided
  * [e] The readout is invalid due to some error; the data should be there but is not for some reason. 
  * [f] The readout is pending and will be provided soon.



## Proposed solution

It makes sense to map the background orientation image to shades of grey, and gas concentration to colour.

However, colour is not a property of light. Perception of colour only happens in the interpreatation layer of the human's visual cortex. 

Experience shows that blending `RGB` values, and even `HSV` values together, does not produce good results; in particular, it will often turn out that the results are confusing. We must not have confusion. 



**It is proposed to use the `CIELUV LCh` colour space to provide the properties required.**



### Starting with the orientation image

The background (orientation) image is converted to `Lightness` only and all colour information is discarded. 

Where the background comes from the TDLidar, it is already to be interpreted as a single-channel `Lightness`. 

	Note that this comes with the problem of scaling, clipping and saturation and  	when mapping back to `RGB` space. This will be dealt with later in this document.

### Adding the gas density image

The Gas saturation is mapped to `chromacity`. Moreover, to emphasize the difference of "safe" from "warning" and "saturation" levels of gas, the `hue` can jump from "yellow" to "red" to "violet". 

* zero `concentration` (0) -> Chromacity of zero. This makes the image default, "safe" image, completely black-grey-white.
* between zero to "alarm threshold" concentration: input values of `concentration` (0.0 ... 1.0) are mapped linearly between `LCh(L,0.01,84)` and `LCh(L,65,84)` - along the `chromacity` axis from `grey` to `strong yellow`.  . The chromacity does not start at 0.00 because of quirks in the `CIE Lch` conversion algorithm that make that point unstable.
* For input values around 1.00, the colour point travels quickly (near-jump) from `LCh=(L,65,84)` to `LCh=(L,65,40)` . From Yellow to Red, keeping the chromactity. 
* Between the "alarm threshold" to "maximum readout": input values of `concentration` (1.0  ... 2.0) are mapped linearly between `LCh=(L,65,65)` to `LCh= (L,100,40)`,  - along the `chromacity axis` from "weak red to strong red"
* Near the end of the scale, the colour jumps again quickly from `LCh(L,100,40)` to `LCh(L,100,332)` along the `hue` axis. Note that this transition needs to be interpolated in the `LCh` space; RGB space would cause werid artifacts.

To summarize, on the ( Chroma, Hue) plane  we have a gas visualisation track:

		[ "[a]",     "[b]",    "[b]",    "[c]",    "[d]",      ]
		[ (0.01, 84), (55, 84), (55, 40), (100,40), (100,323), ]



**Figure: Chroma-Hue map**, proposed trajectory

`Chroma` is up; `hue` is right; `lightness` = `const 56` . `Purple`="this color is impossible in RGB space"

<img src="trajectory_chroma-hue.png">



### The `Lightness` clipping dillema

Increased chromacity always comes with reduced dynamic range on  `Lightness`. In other words, the lightness is limited, from both ends, depending on both `hue` and `saturation`.

For example, one cannot have "completely dark yellow" and "completely bright yellow". In order for yellow to stay yellow, it must be neither black nor white. 
<table style="width:80%;   margin: auto; text-align: center;
  vertical-align: middle;"><tbody>
<tr >
	<td style="border:0.1em solid black;">Chromacity-lightness map for hue=40</td>
	<td style="border:0.1em solid black;">Chromacity-lightness map for hue=85</td>
</tr>
<tr>
<td style="border:0.1em solid black;"><img src="CL_map,h=85.png" style=";" /></td>
<td style="border:0.1em solid black;"><img src="CL_map,h=40.png" style=";" /></td>
</tr>
</tbody></table>

Note that the peak `chroma` occurs at different `lightness` for `yellow` and `red`; In other words, fully saturated `red` has different brightness than fully saturated `yellow`.



This presents us with a dillema. Since the `Lightness` is driven by the input 'orientation' image, and ideally, it should cover the full range of [0...100]; and  it is impossible to correctly reproduce the full range at `chroma=55` and even less so at `chroma=100`. The requirements are to give the gas saturation (chroma) higher priority. Hence, `Lightness` has to give. 

There are at least 3 possible approaches to this problem:

* Saturate the output `Lightness` as per `chroma` limit dictates. This means that highlights and shadows become clipped and unreadable. This would provide most neutral looking image, but at the cost of loosing detail, and possibly orientation, in the affected areas.
* Scale the input lightness at the conflict pixels only. This would translate the full dynamic range of the input to the output, however, it may result in very unnatural-looking artifacts. Still, as long as they provide 'orientation', it might be acceptable.
* Scale the input lightness to a narrower range on the entire source lightness image. This would make the orientation image always both washed out, greish and dark and reduce the contrast; it would not create stark artifacts but the detail would be lost in high chroma areas anyway.

For the initial implementation, we will have the option 1; with other options to be user-selectable.

Ultimately, we want the gas measurement readability to take precedence over the orientation image, hence some concession for loss of orientation have to be made.

Fortunatelly,

the "yellow" range has a fairly large luminosity range, meaning that the gas overlay will remain mostly transparent. Then, the "red" range is progressively less transparent the more saturated the colour becomes, and "violet" has only a narrow luminosity range, meaning that it dominates the background.



### Complementing with "special cases"

After using up the hues of `yellow` and `red`, we are left over with complimentary hues: `green` and `blue`. These are distinct from yellow/red enough to not be confusing. 

* **Positive** special cases shall be served with  `green`, hues around `h=292` with somewhat low saturation of `c=30`
	* for selection masks selecting the ROI. 
	* Progress indicators -- e.g. "this is where we will scan next"
	* It could also be used for cursors, or temporary text overlay that does not obscure the background.
	* a range of hues and chromas near 292 could be used to indicate sub-features, e.g. tables, borders, reticles, crosshairs, e.t.c.
* **Negative** special cases shall be served with `blue`, hues around `h=140`
	* Camera error, laser error, data error e.t.c.
	* ROI position unreachable - user configuration error.



### Summary



**Table 1:  `CIELUV LCh` reference values**

Note that the colour example panels are very approximate due to how human perception works in presence of other colours. 



<style>
td{
  text-align: center;
  vertical-align: middle;
  border-collapse: collapse;
}
.c{  }    
</style>
<table style="margin:auto;"><tbody>
    <tr>
        <td class="comment">State</td>
        <td class="input" style="width: 15%;">`gas detection` scaled value</td>        
        <td class="output-chroma">Output Chroma</td>
        <td class="output-hue" style="width: 15%;">Output Hue</td>
        <td class="L-range">Lightness limit<br/>if no clipping</td>
        <td class="png_example">example colour</td>
    </tr>
    <tr>
        <td class="comment">[a] No detection. Background image visible in full.</td>
        <td class="input">0.00</td>        
        <td class="output-chroma">0.01</td>
        <td class="output-hue">84</td>
        <td class="L-range">[0.1 to 99.0]<br/>background image perfectly visible</td>
        <td class="png_example"><img src="color_no_gas.png"><br/><img src="color_no_gas_2.png"></td>
    </tr>
    <tr>
		<td class="comment">[b] Some gas detected</td>
        <td class="input">[0.00 to 0.99]</td>
        <td class="output-chroma">0.1 to 55.0</td>
        <td class="output-hue">84</td>        
        <td class="L-range">shrinks with input</td>
        <td class="png_example"><img src="color_little_gas.png"></td>
    </tr>    
    <tr>
        <td class="comment">transition-low point</td>
        <td class="input">0.99</td>
        <td class="output-chroma">55.0</td>
        <td class="output-hue">84</td>            
        <td class="L-range">[49.5 to 87.8]</td>
        <td class="png_example"><img src="color_gas_warn_threshold.png"></td>
    </tr>
    <tr>
        <td class="comment">transition [b] to [c]</td>
        <td class="input">[0.99 to 1.01]</td>
        <td class="output-chroma">55.0</td>
        <td class="output-hue">[84 to 40]</td>                
        <td class="L-range">1st hue jump band</td>
        <td class="png_example"><img src="color_gas_hue_jump.png"</td>
    </tr>
    <tr>
        <td class="comment">transition-high point</td>
        <td class="input">1.01</td>
        <td class="output-chroma">55.0</td>
        <td class="output-hue">40</td>            
        <td class="L-range">[49.5 to 87.8]</td>
        <td class="png_example">N/A</td>
    </tr>
    <tr>
        <td class="comment">[c] Notable concentration of gas </td>
        <td class="input">[1.01 to 1.99]</td>
        <td class="output-chroma">[55.0 to 100]</td>
        <td class="output-hue">40</td>
        <td class="output">shrinks</td>        
        <td class="png_example"><img src="color_gas_notable.png"</td>
    </tr>
    <tr>
        <td class="comment">endpoint of [c]</td>
        <td class="input">1.99</td>
        <td class="output-chroma">100</td>
        <td class="output-hue">40</td>                
        <td class="L-range">[50 to 56] very narrow, the background image is essentially covered.</td>
        <td class="png_example"><img src="color_gas_endpoint.png"</td>
    </tr>
     <tr>
        <td class="comment">[d] Too much gas, instrument saturated</td>
        <td class="input">[1.99 to 2.00]</td>
        <td class="output-chroma">[100 to 100]. <br/> 100 at the end points, but follows the saturation limit on the way</td>
        <td class="output-hue">40 (down to) 323</td>
        <td class="L-range">2st hue jump band</td>
        <td class="png_example"><img src="color_saturation.png"</td>
    </tr>
    <tr>
        <td class="comment">[e] Special: device error</td>
        <td class="input">N/A</td>
        <td class="output-chroma">30</td>
        <td class="output-hue">140</td>                
        <td class="L-range">[17 to 95] background image well visible</td>
        <td class="png_example"><img src="color_blue_mask.png"</td>
    </tr>
    <tr>
        <td class="comment">[f] Special: Data pending or mask</td>
        <td class="input">N/A</td>
        <td class="output-chroma">30</td>
        <td class="output-hue">292</td>                                
        <td class="L-range">[3 to 81] background image well visible</td>
        <td class="png_example"><img src="color_green_mask.png"</td>
    </tr>
</tbody></table>




## Implementation

### Theory of operation


#### Colour map selection

There shall be no need to change the colour map. The data we are displaying is the same kind at all times.

Since the system depends on a colour to deliver information, no provision is made for colour blind people.

The initial colour map is hard-coded; additional colour maps could be loaded from a dedicated json file generated outside the system.


#### Value to colour mapping overview


![2022-02-14-io-values-mapping.drawio](2022-02-14-io-values-mapping.drawio.png)

#### `Background` input value mapping

The input lightness of arbitrary value is first autoscaled to the range of  ( `input_l_low`, `input_l_high` ) to ( `output_l_low` , `output_l_high` ) where 

`input*` values are automatic min/max of the data, and `output*` values are `_low` = 0.1 and `_high` = 0.9; This is to reduce the emergence of clipping, and is adjustable



**API parameters:**

```c
(bool)input_light_autoscale = true

float input_light_low;

float input_light_high;

float output_light_low = 0.1f;

float output_light_high = 0.9f;

```



#### `Gas detection` input value mapping

The input `gas detection` value is scaled to 2 bands: `band_some`  and `band_lots` .

The `band_some` scales to fixed values of (0.0 to 1.0 ); the `band_lots` scales to fixed values of (1.0 to 2.0 ).

There is no autoscale option. It is intended that the values are configured by the operator for a given measurement scenario; this is to avoid ambiguity as to how bad the leak is.

**API parameters:**

```c++
float input_gas_some_low;

float input_gas_some_high;

float input_gas_lots_high;

float output_some_to_lots_margin = 0.1; //decides the width of the h 

```



Any input value below `input_gas_some_low` will map to "zero chromacity"; any input above `input_gas_lots_high` will map to "gas detection overload" 

#### Special values

`background` value of `NaN` maps to the `device error` colour and has precedence over any other value.

`gas detection` value of `NaN` maps to the `Data pending or mask` colour

#### Future extensions

In the future, negative values of `gas detection` may be mapped to addional categories of `data  pending or mask` 

Alternatively, additional input channels could be used.

---


### API Summary

**JSON configuration file**

```json
  "ImageShader1":
  {
    "verbose": true,
    "input_light_autoscale": true,  // the "intensity" data is scaled to `output*` range, automatically.
    "input_light_low": 0.0, // ignored if `input_light_autoscale` is true.
    "input_light_high": 1.0, // ignored if `input_light_autoscale` is true.
    "output_light_low": 0.1, // lightness output range. Valid range 0..1, 
    "output_light_high": 0.9, // lightness output range. Valid range 0..1, 
    "input_gas_some_low": 0.0, // band 1 bottom edge
    "input_gas_some_high": 300.0,// band 1-2 edge
    "input_gas_lots_high": 1200.0,// band 2 top edge
    "output_some_to_lots_margin": 0.05 // band 1-2 transition width
  }
```



**HTTP API Endpoints**

```bash
wget http://127.0.0.1:8001/api/v1/composite/image -O composite.jpg

curl -X http://127.0.0.1:8097/api/v1/parameter/set 

curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"imageShader.verbose", "delta":true}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"imageShader.input_light_autoscale", "delta":true}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.input_light_low", "delta":0.0}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.input_light_high", "delta":1.0}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.output_light_low", "delta":0.1}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.output_light_high", "delta":0.9}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.input_gas_some_low", "delta":0.0}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.input_gas_some_high", "delta":300.0}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.input_gas_lots_high", "delta":1200.0}'
curl -X POST http://127.0.0.1:8097/api/v1/parameter/set -d '{"path":"pixelShader.output_some_to_lots_margin", "delta":0.05}'


```









[TOC]





