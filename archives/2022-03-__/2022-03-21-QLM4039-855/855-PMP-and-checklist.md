# PMP template

for small projects



Integration

→ Charter, Plan, Change docs

Scope

→ WBS doc

Schedule

→ Schedule doc

Cost

→ Budget doc

Quality

→ Requirements doc

Resources

→ This is for Brieuc.

Comms

→ All documents in Notion.

# Risk register

| ID   | name | raw-severity | raw-likehood | actioned-severity | actioned-likehood |
| ---- | ---- | ------------ | ------------ | ----------------- | ----------------- |
| 1    |      |              |              |                   |                   |





# Procurement

none



# Stakeholders

Me, Charles, John







# Legend

❓ = hypothesis, for consideration

➡️ = possible action to try

✅ = finished, success, no need to revisit.

❌ = finished, did not help, no need to revisit. 

❗️- started, but no success yet due to some (likely) solvable problem on the way

🌱= ready to start

⚠️= not ready to start

⬆️ = in progress at the time of the last update. No result yet

📞 = decision needed

