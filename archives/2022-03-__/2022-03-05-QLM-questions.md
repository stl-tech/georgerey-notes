 

* The forward and backward coordinate transformations seem unimplemented
* My CLIon crashes during build with no error message, just a crash :-( 

---



Main problem:

* Requirement: Smooth transition between scan pattern.



* Current limiting on the motors



* Spiral implementation is not so good, not interruptible.

  * Leave this for now, altogether. 

  

* The paper has another pattern which is a set of loops: The Dynamic Zoom. 

* [this task] Implement the The "dynamic zoom"

  * Drive the motors first, do not care about target coordinates much
    * only 
  * Check the extent of the FOV
  * Check the acceleration limits
  * End of Task.

* The zoom must be adjustable on the fly with smooth transitions

  * Use the existing trajectory transition logic
  * Consider using a periodic parametric trajectory driver with smooth parameters rather than trajectory

* Desirable: Produce an "OK flag" when on stable trajectory

  





Testing?

- Analysis:
  - the ServoSim can output positions, and effective accelerations 
  - [separate task] Density of coverage
  - [separate task ] Refresh rate at given point (how old is the point for a given pixel)
  - [separate task] Possibly new renderer later on -- using delaunay zones
  - 