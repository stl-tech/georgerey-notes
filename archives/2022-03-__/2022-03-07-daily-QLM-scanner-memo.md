# Memo: The motion control architecture



## General note

Programming a motion control system is a little bit like programing a parallel processing task, where you always have a race condition and you always operate with outdated information. 



## The current situation

Under following assumptions:

* The current on the motors needs to be hard limited to prevent power supply brownout
* The application demands virtually continuous movement of the motors, stops are rare.
* The operation needs to be interruptible without violating any of the safety constraints
* The motion needs to be vibration free-smooth
* The scan pattern needs to be reconfigurable on the fly



Under current design, where we have a "trajectory generator" that emits points on demand by outer loop, 

**I submit that this is an entirely wrong architecture to do motion control.** 



In the current architecture there will be inevitably "set point chasing" behavior of the current control loop.

The reason is that the current-control loop of the ODrive runs at 8kHz; meaning that commanding a new motor position at any less than that rate means that there will will result in a "set point chasing" behavior of the current control loop. 

Moreover, the closer the achieved point is to the commanded point, the worse this effect gets. For example, the winding current controller might get commanded to actively brake the motor just before receiving a new command to continue the movement. This is quite bad. 

Low-pass filtering helps, but does not solve this problem: If the trajectory generator outputs 100 points per second, and the 1st order low-pass filter is set to -3dB@20Hz, then it means that there is still a 50% step size at 20Hz, 25% step size at 40Hz, 12.5% at 80Hz, and still as much as 6% at 160Hz and so on, in other words, the high frequency jitter is unavoidable. Raising the order of the filter is problematic because there will be a trade-off between things like overshoot, ringing, group and phase delay, and so on. In other words, low-pass filtering is a last resort and not a desirable solution to get smooth motion. 



## The correct solution



The Linux based computer commands not the points to get through, but rather the kind of trajectory to follow, and it's parameters. 

For the case of the "dynamic zoom", the parameters are:

* The outer radius of the screen-space scan area. 
* The desired refresh period of the innermost area.  (e.g. 30 seconds) 

That's all. 



The motion control and trajectory is integrated inside the real-time control loop inside ODrive, and run at the full 8kHz. 



Odrive CPU, runs several layers of control in a single loop. The loop is driven with a hardware timer at fixed rate (8kHz)

The **First Layer**, takes in the `desired trajectory properties` command and  interpolates the parameters of the trajectory (outer radius and refresh rate) between the old one and the newly-commanded ones. The time constant might be 0.1sec here (800 time steps)

The **second layer** the trajectory that is defined by a static periodic function, is evaluated at current and next time step using the already-smooth `trajectory parameters` and `time`. The trajectory generator outputs a pair of "previous" and "next" timestamp, (point, velocity and acceleration) vector. The 'real' location is somewhere between the "previous" and "next" points, but it in this layer, it should be understood that neither "previous" or "next" points are actually achieved. At this point, the tracking error (distance between the desired trajectory and actual trajectory) can be computed and reported.

In the **3rd layer**, velocity control happens. The objective is to chase the future-time location on the trajectory. 

1. the `current location` and `current velocity` of the motor, and the `previously commanded acceleration` is used to compute (extrapolate into the future) the future position of the motor  This results in a future "wrong position"

2. The error between future wrong position and the `desired position` and `desired velocity` is calculated

3. The **jerk** required to correct the future position is calculated and then clamped to limits.

4. The **new acceleration** command is computed by adding the jerk to the previous acceleration command.



The **4th layer** takes in the `acceleration command`, 

The acceleration command is translated into **torque demand** (static linear relationship) and then, the `torque demand` is translated into **current demand** .

The `current demand` as a function of `torque demand` is a highly nonlinear, position dependent problem for BLDC motors! There are a couple of control schemes here, such as Field-Oriented Control (FOC), Voltage-Frequency Control (VFC), and more. For very slow motion, direct voltage control (DVC) (using winding resistance to convert voltage to current) yields the lowest vibration, and is very simple too. 

FOC is vastly more complicated than anything else due to the need to measure current at tightly defined times; it is used in practice because it is the most energy efficient way to drive high power motors to their absolute peak performance, where the load can change rapidly -- e.g. in an electric sports car.

The VFC is good if a motor is well specified for the application and runs at peak power and constant velocity most of the time. 

If we need a maximally smooth motion with minimal vibration, and can accept the lower efficiency, then the DVC is the right answer.  



Finally, **in the 5th layer**, the motor winding current is controlled by a P or PD type controller on the duty cycle of the H-bridge. At this point, the peak current, and current ramp can be limited. 



Note that, assuming the 4th and 5th layer are fast enough and correct, then it's the  *jerk limit* that decides the time-spectral content of the actual position of the motors. 



All the while, the Linux Computer which runs the user interface, network interface, separate web server, runs more threads than there are cores, flash disk operations, heavy computation (curve fitting, rasterisation), logging etc -- **should not be used** for any real-time control. 

