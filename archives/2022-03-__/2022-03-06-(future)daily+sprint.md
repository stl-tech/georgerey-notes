Re: STI34087DEV064 "XMPP: Implement a local XMPP server" -- maybe you mean "Implement a local user agent for testing purposes". This is done, and working now.

My opinion at this point is that we simply do not need a local XMPP server all that much. We can use our customer's XMPP server and the development accounts right away. It is fast enough, safe enough, and more relevant re the final product.



| Role        | Account name                                 | password         | registered |
| ----------- | -------------------------------------------- | ---------------- | ---------- |
| observer    | george@chat.planeti.co.uk                    | EuY6gs3getVY     | OK |
| auth user 1 | surestop-user-1@chat.planeti.co.uk           | 57d76NxHqFYy     | OK |
| auth user 2 | surestop-user-2@chat.planeti.co.uk           | 7H7ef3jmzvQF     | OK |
| admin 1     | surestop-admin-1@chat.planeti.co.uk          | Na1Uu6czN3pm     | OK |
| admin 2     | surestop-admin-2@chat.planeti.co.uk          | pY56qUltFPJf     | OK |
| reserve     | surestop-reserve-1@chat.planeti.co.uk        | xE686Rfy1Z9m     | OK |
| hub-1       | 44371917305fa5e90c24261@chat.planeti.co.uk   | 7167eb0abad8ed2f | OK |
| hub-2       | 44371916a780a3f6b5ec47b@chat.planeti.co.uk   | f638ed0983d0b57d | OK |
| hub-3       | 44371919f13c4a586001709@chat.planeti.co.uk   | bb0e00a9a15cb8b5 | OK |



Invite link
https://chat.planeti.co.uk/invite/84eYgKTKbr-rwm8RXzjidz_n/

**Done:**

- [x] Organise the functionality into layers:
- [x] Constants: named constants, enums,  internal message typing
- [x] Base:  deals with asyncio guts,
- [x] Middle: Deals with XMPP library, XMPP events, provides executable primitives for the business logic
	- [ ] [80% done] Mini-roster: deals with hub-side xmpp accounts and comand authorisation
	  - [x] Recognize unknown contacts
	- [ ] [tests & validation pending] Persistent event log buffer
	- [ ] [50% done] Settings store and update
- [x] Console: Provides business logic interface, encapsulates middle layer in a thread and animates it.
- [x] Generate accounts
- [x] Install engineering accounts on our XMPP server
- [x] React to the presence
- [x] React to message
- [x] React to internal command queue with params
- [x] Work indefinitely, non-blocking in a thread
- [x] Buffer events in a local disk-backed queue
- [x] List contacts
- [x] Solve a problem where a contact that is "in a circle" cannot be removed from the server, no matter what
- [x] Remove contact
- [x] Add contact
- [x] Remove all contacts
- [x] Stream events to console
- [x] Load admin name and obs name from config JSON
- [x] Reconnect if initially couldn't connect
- [x] Re-do account names for hubs, include the prefix
- [x] Remove all unknown contacts 
- [x] Remove all contacts, including admin and obs, altogether
- [x] Move from `conda` to `venv`
- [x] Ensure python 3.7.x in the `venv`
- [x] Verify and minimize dependencies for the new `venv`
- [x] Clean up and prepare the first pull request, then merge
- [x] Persistqueue :: see if I can run it as pip install package, rather than having source code included
- [x] SLIXMPP :: to package, remove source from primary source code tree
- [x] Move out the src.pyutil to a path folder, rather than specify long path. Use as package, not as source code
* [x] Check if UUID is any good for purpose of account generation. 
  * [x] Note that the accounts are already generated and there is no need for it anymore.
  * [x] The UUID is does not reduce the account generation code; i use `account=os.urandom(8)` which is already crypto secure.
* [x] Remove the system event persistqueue. It is not needed. There will be a system-wide log sink on the target platform.
* [x] New name: "XMPP" use that as the top level interface name
* [x] Build an application level interface with simple commands and observables
* [x] Remove the account generator
* [x] Reduce the readme as per comments in the PR
* [x] do not check in my checklist - use jira instead
* [x] review "setup.py" to list `Python>3.7`
* [ ] ~~Remove Admin and Observer users from the `miniroster`~~
  * [ ] Outcome: This is delayed. They can stay for now until the testing at the application level is reliable.

* [x] Construct the user-side object on the application level

**To Do**

* [ ] Implement the connection enable/disable functionality
* [ ] Move the secrets.xmpp.hub_id identity and password to just one file, rather than both config and secrets.
* [ ] Add a test to see that it does start and stop as desired, and that it produces the appropriate events
* [ ] Rebuild the test infrastructure (the user simulator) to use the same client as from the application
* [ ] React to registration request: Add unique user  via internal API only if there is no user
  * [ ] Accept incoming user subscription via any of XMPP IQ or Presence channel, but only if there is no user
  * [ ] And, produce the event
  * [ ] And, save the new name to file in `etc`
  * [ ] 
  * [ ] And, check that file on load
  * [ ] If there is an user, send a short but polite decline.
    * [ ] Note that this is an attack surface for the SureStop to handle. Could be used for resource discovery and DOS.
* [ ] Review the use case diagrams and based on that, review the to-dos
* [ ] A given hub should talk to one server for XMPP and one server for Historical data logging. Add configuration details.
* [ ] React to registration request: Reject adding user if there is a user
* [ ] React to reset request: remove user
* [ ] Save the settings that have been changed runtime
* [ ] Local settings file overrides the server provided setting (on change, makes an authorized user, unauthorized)
* [ ] on boot, if there is no authorized user, set state, raise an internal event,  send a notification to the admin and observer
* [ ] Reconnect on connection loss (non-initial failure, after a while failure)
* [ ] Accept a REPL command if it comes from an authorized user
* [ ] Accept a REPL command if it comes from a known admin
* [ ] Accept a REPL command if it comes from an unknown user but with a good key
* [ ] Send log events to Log sink
* [ ] Send log events to XMPP PEP

[out of scope] accept local connections on a TCP port

  

----

**Tickets:** https://stltech-client.atlassian.net/jira/software/c/projects/STI3087DEV/boards/3

**Wiki:** https://stltech-client.atlassian.net/wiki/spaces/STI3087/pages

**git:** https://bitbucket.org/stl-tech/sti_surestop_hub











