Studying [[QLM4039DEV-469\] Improved scan controller code - Jira (spectrumhosting.net)](https://stl-tech.spectrumhosting.net/jira/browse/QLM4039DEV-469):



**Scope of the problem:**

* Q: Is the job to make *any* pattern programmable, or the Spiral pattern in particular?
  * A: We start with the "dynamic zoom" pattern, and then we have to implement all patterns as per the document
* What's wrong with the current spiral? You mentioned it is "hard to read"?
  * A: n/a

* Q: You mentioned that the problems are vibration and overcurrent -- please tell me more:
  * Q: Are there hard limits or merely "minimize"
  * Q: What are the hard constriants, refresh rate? constant linear velocity?
    * Q: C.A.V. might be important for the measurement (light integration) process to be consistent
  * A: The hard limit is the motor current, as the power supply is overwhelmed
  * A: The vibration is not a primary concern just yet
* Apart from "As little as posible", is this an output-quality driven job or fixed-time job? in both cases, 
  * A: n.a.



**The equation:**

* Q: Is the equation verified to be correct?
  * A: It appears to be OK for now.
  * A: There is already forward and inverse kinematics implemented in the branch `QLM4039-474-prototype-trajectory` - go and find it, start with `MotionSegmentImpl.hh`

 * Q: It appears to be an approximation that skips the thickness of the prism, distance between the prisms, and the possibility of having the laser beam enter off-centre (which it should) -- are the approximations verified to be OK?
   * A: That's OK for now
 * Q: How about using complex numbers to represent orientation? (that avoids numeric instability at the wrap-over)
   * A: ?
 * Q: The equation uses polar and azimuthal coordinates, where is the conversion to screen coordinates?
   * A: There is already forward and inverse kinematics implemented in the branch `QLM4039-474-prototype-trajectory` - go and find it, start with `MotionSegmentImpl.hh`



**The hardware:**

* Q: Are the prisms heavy? under 1Hz =60RPM continous rotation, do you get optically relevant vibration of the casing?
* Q: Are the motors like super-whimpy and they will not do a 1Hz sine with 180deg. swing under existing inertia load?
  * A: The motors are good, but there are input power constraints
* Q: If low-frequency vibration is a concernt, can we add reaction mass?
  * Q: Can we add a reaction-gyro? a 300g gyro can be made to have very high rotational intertia, effectively absorbing the reaction forces
  * A:?
* Q: You mentioned that the 17-bit encoder outputs a floating point number of absolute count of rotations? surely that leads to loss of precision after only 64 rotations, or around 1 minute @ 1Hz?
  * A: The encoder is vastly overspecified. Likely, 14 bits is plenty enough, meaning that we have 9 bits (512) rotations before it may become a concern
    * Q: This is still not very many!



**Pattern: **

* Q: The scan patterns studied in this document are not the only scan patterns possible. What about curvilinear matrix?
  * A: N/A

* Q: It appears to me that for purpose of **spatial concentration measurement** and not merely constructing a range image -- one would either desire a constant spatial velocity of the beam or at least know precisely what it is to take that into account when integrating light
  * A: Variable velocity seems to be OK for now
* Q: The operator will expect the **true** image resolution to be uniform, and will need special indicators if it is not
  * A: N/A
* Q: The operator will expect the instrument sensitivity to be uniform, even if the noise distribution is not uniform
  * A: N/A
* Q: I can see that the TDLidar sampling rate is 40Hz, this means that in 1 minute, you will get at best 2400 usefull source points per minute and at best 49x49 pixels in an image per minute; this might be enough   for **measurement** 
  * if we have averaging for 10 minutes, that's still only 24'000 measurement points or at best 150x150 image

* Q: Is the target refresh rate known, as this should come from the needs of the application?
  * Q: Is there a need to have sectors (high-refresh rate & low-refresh rate areas)
  * A: For now, we got asked to implement the patterns as specified in the document
    * Q:  Maybe we can propose to them to research uniform-density, uniform-sensitivity, refresh-rate-controlled patterns? I can do that!



**Some tech notes:**

* The ODrive PID loop rate is 8KHz, ( current true bandwidth ~ 500Hz)
* Can do torque, velocity, position, trajectory, and filtered P2P
* Currently likely the P2P mode is used, even though
  * Q: For this application, it seems that the velocity control might be more appropriate?
    * Q: Or, even, moving the forward/reverse kinematics AND pattern control, to the ODrive processor and adding a custom protocol? I hear it has spare power for this, and it's an RTOS



===

Key limit is the power supply

"Optical kinematics " class - gives screen coordinates from motor coordinates

Need to set scan area radius

"Motion Segment "

* T