

Work

-- Augument ServoSetSim to output beam location using `UseComponent` `OpticalKinematics`

-- Reduce one of the `doScanPattern` to reduce the count of executables

-- keep cleaning up the config files as you go



* Clean up:

  * [ ] Bedroom
  * [ ] Bathroom

* Shopping:
  * [ ] bigger insole inserts. Return the one not-opened that is too small

  

  

QLM: 

PR: Synchronous zoom: https://bitbucket.org/stl-tech/qlm_application/pull-requests/539

PR: JSON Files update: https://bitbucket.org/stl-tech/qlm_application/pull-requests/543





STI progress:

The pipeline fails for a very strange reason. Try building my own image with pre-built installed thingies.

https://bitbucket.org/stl-tech/sti_surestop_hub/pull-requests/10



Home:

-- Close up the paperspace-reymonte

-- open-up the Home.pl reymonte-H

-- ask home.pl why can I not use the certificate from lets encrypt

147.60PLN

Hostig bizness - 40PLN/miesiąc na 2 lata = 960PLN

​	-> 50pln/miesiąc

they provide:

​	-> good, hosted emails

​	-> php, database, & file serve, albeit with complicated interface, 

		* file serve is somewhat useful, sftp and http access, but no https(!)
		* databases - OK.
		* php is not useful at all, I will not be learning that
		* wait, it says there is an SSH access?? let's check that!

====

atNow:



![image-20220316140542923](/home/mib07150/.config/Typora/typora-user-images/image-20220316140542923.png)

![image-20220316140614233](/home/mib07150/.config/Typora/typora-user-images/image-20220316140614233.png)

[0, 1] are positions

[2, 3] are velocities



I think I tracked a down the entry-point of the problem:
the spline transition never hits the target point close enough at the end of the transition time, and that triggers creating a new transition, which, due to how the previous target  trajectory being unachievable, is also unachievable

It tries to go from (M0Pos,M0Vel,M1Pos,M1Vel) = (0.01, 0.01, 1, 1.005) to (0.00, 0.00, 1.00, 1.005) 

The previous tests would pass because previously, the "target and achieved" position were "close enough"



This might be because the spline's clock does not go from 0.00 up to 1.00 but rather from 0.00 to 0.99, 

in other words, this is an example of an offset-of-1 bug :-(

I might need to modify the Spline generator to advance it's internal time a bit faster, so that the end point and velocity does hit the target at `t=final_t-dt` rather than at `final_t` 



===

![image-20220316150619930](/home/mib07150/.config/Typora/typora-user-images/image-20220316150619930.png)

So there is still something quite wrong with either the ScanControllerMotion or the Spline computer.

Or with the velocity computer





====

OK, here is the plan of action:

--- get the numerical values of source and target point for the spline

--- test in a separate tester how far off are they are from desired

--- add the position&velocity history somewhere, and trap sharp corner



Example of transition tests:

(0, 0, 0, 0, 0, 0 ) to (0, 0, 1 ,1.01232588, 0,0)  -- fails on velocity

```
MotionSimpleSplinePVA
t_dur = 2
p0 = 0
p1 = 0 
v0 = 0 
v1 = 1.01232588
a0 = 0 
a1 = 0
t_start = 0

computes to: 
m_d3adt3 = -22.7773323
m_d2adt2 = 21.2588425
m_dadt = -6.07395458


```



at t=301:

Transition desired from:

(0.0078125, 0.00800509192, 1,1.01232, 0, 0) to (0, 0, 1, 1.01232588, 0, 0) at t=357 with timeDelta = (1/128)

unstable spline 

64 bits : let's say 40 bits fraction and 23 bits of int + sign



=======================

First transition at t=100 -- expected, as this is transition from "stop" to "SYncZoom@0.00f". Spline is injected.

second transition at i=356, expected, as this is end of the spline.

at i=356:



currentMotion(previous) = spline, transitionTarget = syncZoom

ScanControllerMotion-currentTime << 0.0f (this is a "phase of motion" time and not "system time"

now, the currentMotion is already new, with time=0



==? Problem wrong key pressed

at i=357, the isClose is tested again????

AND fails with

M0Pos = 0.0078125 and M1Pos = 0.00000, delta=0.0078125>0.0010000  :-()



===

FOR TOMORROW:

CHECK WHAT IS HAPPENING AT i=356 and i=357. Why is transition happening 2x???









----

