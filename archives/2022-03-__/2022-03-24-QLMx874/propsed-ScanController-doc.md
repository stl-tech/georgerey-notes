Docs for the ScanController2

The areas affected with the change:



![image-20220324203212368](/home/mib07150/.config/Typora/typora-user-images/image-20220324203212368.png)



Assumptions:

* Uses integers as far as practicable,
  * For time
  * For position, e.t.c.
* Uses MotionSegments
* Provides good debugging and test points
  * Which are used



=== use of integers

The circular position of the motor is always expressed as full range of signed int32

* meaning, 1 rotation = full round from 0, +1 to -1 and back to 0.
* This provides circular domain arithmetic for free, no overflows ever
* Good performance on small chips
* Easy downgrade to 16-bits if we need to store larger amounts of data
* Simple multiplications
* Division by constant is a multiplication and shift. The inverse constants can be pre-computed.

Downsides:

* need to be careful to provide sufficient precision -- avoid small numbers.
* Some operations will require promotion to 64 and 128 bits, e.g. for spline generation



===

need:

-- new servoSet,

​	-- new ServoSetSim

​	-- new ODriveServoUSB, ODriveServoCAN

=== Review the std::chrono documentation

 - can it get 1MHz time from the OS
   - there is a `system_clock` , `steady_clock` and `high_resolution_clock`
   - There is `duration` which might be better for us as it deals with time since powerup, and does not need to depend on system clock while in simulation mode
 - does it do time differences
 - what are the idioms to convert to int64 nanoseconds

https://www.geeksforgeeks.org/chrono-in-c/





Need first:

-- new EvetDelayTiming -- it is currently based on <<double>> which we know is not available on the target platform. 

	* Write a new one, much simpler, "Sleep until next time slot", AND based on int64
	* 

Questions for Charles:

-- How do you feel about me introducing new words, and keeping a glossary. I have experience doing this and it was always good for communication and productivity. 

=============



New design for ScanControllerMotion2::ScanControllerBase

methods:



# Methods that will behave the same way as in ScanPatternMotion:
```
start(); // starts doFieldScan in a thread;
stop(); 
isRunning(); 

servo2 get_servos(); 
void setScanPattern(ScanModeT kScanPattern)

virtual bool updateMotion();
virtual void doFieldScan() = 0;

```

# Methods that will behave differently or are depercated: 
```
virtual void doSetScanPattern(ScanModeT kScanPattern) = 0;
deprecated virtual bool setScanRadius(double radius) -- gives outer radius for spiral ring, outer valid radius for star, fails for Transition

//! deprecated - 
deprecated nextPoint()
```



# Methods that will return a hard error:
```
std::vector<float> doLastMoveRequest()
```



# New methods:



```

//! waits a variable amount of time, aims to return on the next time slot. Uses thread:: to release the CPU to deal with other things in the meantime. 
YieldUntilNextTimeSlot() 

//! returns the last waypoint sent to the servos
getLastWaypoint()

//! preview of the waypoint for provided time
peekWaipointAt(future time)


    
    

```



# New types

```
MotorWaypointT;

```



===

Q: Why can I not simply change the type of time in the existing class?

A: Because there are long-ranging dependencies there: on

​	-> EventTiming

​	-> Servos

​	-> MotionSegments

​	-> http API outputs

Who knows what else would be affected.

When thinking about it, I realised that the new code does not have to be, and won't ever be equivalent to the previous code. Float positions and time are inherently not equivalent to integer ones.

Hence, there is no need to demonstrate equivalence. There is only a need to demonstrate satisfactory test suite for the new system.

The new system will use integer-based motions, integer-based time; it will be more encapsulated, and will have less backward dependency ties.

Most of the old code can be adapted nearly-as-is; so it's actually less work than you might think.

----



