https://stltech.atlassian.net/browse/QLM4039-874 use std::chrono for ScanControllerMotion and MotionSegments

for review (checklist)

* [x] `MotionSegment.hh` 
* [x] `ServoSet.hh`
* [x] `ScanControllerBase.hh`
* [x] `ScanControllerMotion.hh`
* [x] `EventDelayTiming.hh`
* [x] `DataMessages.hh`  (very big file)
* [x] `MotionInterpolator.hh`
* [x] `MotionSegmentImpl.hh`
* [x] `MotionSegmentSynchronousZoom.hh`
* [x] `ScanPatternBaseClass.hh`
* [x] MotionSegmentImpl.hh (skipped, depends on MotionSegment.hh)





## Change list:

### For `float time`


* **`MotionSegment.hh`** :
* [ ] `Eigen::Vector2f MotionSegment::at(float t) const`  - uses `float time`  | 12 usages
* [ ] `TrajectoryPointT MotionSegment::computeTrajectoryPoint(const std::array<Eigen::Vector2f, 3> &pnts, float delta)` -- uses `float time` | 2 usages
* [ ] `TrajectoryPointT MotionSegment::trajectoryPoint(float t) const` -- uses `float time `  | 13 usages


* **`ServoSet.hh`**
* [ ] `virtual bool movePoint(const TrajectoryPointT &positions, float timeDelta);`  -- uses `float time` | 3 usages


* **`ScanControllerBase.hh`**
* [ ] `virtual TrajectoryPointT nextPoint(float delta)` - uses `float time` | 3 usages


* **`ScanControllerMotion.hh`** 
* [ ] `ScanControllerMotion(std::shared_ptr<TDLidarN::ServoSet> kServoSet, float timeStep, float transitionTime, float maxiumMotionDuration);` -- uses `float time` , | 1 usage
* [ ] `TrajectoryPointT nextPoint(float delta) override;` -- uses `float time` | 3 usages
* [ ] `float m_maximumMotionDuration = 1024.0f; //!< Maximum duration for any motion, used to prevent precision problems.` -- stores `float time`  | 2 usages
* [ ] `float m_transitionTime = 5.0f;`  - stores `float time` | 6 usages
* [ ] `float m_currentTime = 0.0;`   -- stores `float time` - 11 usages


* **`EventDelayTiming.hh`**
* [ ] `[[nodiscard]] static double dTimeNow();` -- returns `float time` , make sure it is deprecated
* [ ] `[[nodiscard]] double stepDelay() const`  -- returns `float time`
* [ ] `void setStepDelay(double interval)` -- accepts `float time` 
* [ ] `double m_nowTolerance = 10.0;   //!< Amount of time in microseconds between sequential calls to the clock now function` -- stores `float time`

* **`MotionInterpolator.hh`**

--- many 



* **`MotionSegmentImpl.hh`**

-- many 



* **`MotionSegmentSynchronousZoom.hh`** 

-- Frontier



* **`SteadyClock.hh` **

-- created.



* **`ScanPatternBaseClass.hh`**

-- Frontier




### For `float position` 

* **`ServoSet.hh`**
* [ ] `virtual bool move(const std::vector<float> &positions);`  -- do not use `std::vector<float> `, uses `float position` | 6 usages
* [ ] `[[nodiscard]] virtual std::vector<float> position();` -- uses `float position`  | 6 usages


* **`ScanControllerBase.hh`**
* [ ] `[[nodiscard]] std::vector<float> lastMoveRequest() const;` -- uses `float position` | 3 ysages


* **`ScanControllerMotion.hh`** 
* [ ] `[[nodiscard]] std::vector<float> doLastMoveRequest() ` - uses `float position` | 1 usage

### Other

**`DataMessages.hh`**

* **`Class ServoState`**
  * DO USE THIS CLASS -- the functionality that is supposed to be here, has been replicated at least 3 times across the code base. Clearly it is needed!
  * specialize constructor `explicit ServoState(const Eigen::Vector2d &position,const Eigen::Vector2d &velocity,ServoUnits units)`
    * One for radians, with floats
    * One for EncoderTicks, as uint16
  * Move internal state representation to `uint32_t` even if the position can be read and written in radians and float turns
  * Add timestamp, use `SteadyTimePointT` type
* **`Class ScanPattern`**  -- need to watch this, as there is a potential for conflict or absorbing it into the upcoming `ScanPatternController` 



# Process

[x] OUT OF SCOPE:: Do not convert `float positions` yet; this is future work; 

[x] CONVERT `float time` to `DurationT` only 
