![ScanPatternController class tree](ScanPatternControllerClassTree.png)
<details>
<summary>diagram source</summary>

```mermaid
classDiagram
    class TDLidar
    class ServoSet{
        +float JerkLimit
        +float AccelerationLimit
        +float VelocityLimit
        }
    class OpticalKinematics{
        +float maxScanRadius
        }
    class ScanPatternController{
    	+currentPattern
    	+patternQueue
    	+desiredTransitionTime
    	+next()
    	}
    class ScanPatternT{
        +MotionSegment[] segments
        +bool periodic
        +at(time t)
        +toJson()
        +fromJson()
    }
    class ScanPatternFactory{
        +spiral(innerScanRadius, OuterScanRadius)
        +flower(innerScanRadius)
        +stop()
        +compositespiralflower(descriptor)
        +optimizeIteratively()
        }
    class EventTiming
    class MotionSegment
    class HttpApi{
    	+requestPreview()
    	+commit()
    }
    
    TDLidar "1" o--> "1" ScanPatternController
    ScanPatternController "1" o-- "1" ServoSet
    ScanPatternController "1" o-- "1" OpticalKinematics
    EventTiming <-- ScanPatternController : uses
    ScanPatternController "1" o-->  "*" ScanPatternT : consumes
    ScanPatternFactory ..> ScanPatternT : emits
    ScanPatternT "1" *-- "many" MotionSegment
    HttpApi <..> ScanPatternFactory
    HttpApi <..> ScanPatternController
    ScanPatternFactory ..> ScanPatternController : knows about
    
```

</details>
