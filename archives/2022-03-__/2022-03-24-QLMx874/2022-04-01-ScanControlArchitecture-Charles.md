## Scan Control Architecture



This document outlines the architecture of the new scan control code.

## Background

The QLM TDLidar uses a laser modulated with a pseudo-random sequence to scan a scene.  

The beam is directed by a set of two wedged prisms which together control the angle of the beam leaving the camera. The wedge angle is 6 degrees, which through two prisms can add up to 12 degrees from the boresight.

To avoid artefacts created by laser speckle the beam should move across the scene at faster than 10m/s and to avoid blur less than ~500m/s    Speckle is avoided where the beam moves fast enough that interference patterns in the laser return change fast enough to average out over the sample period.

The scan pattern generation and control can be done independently from the return sampling as the FPGA captures the motor positions directly when it takes a reading.

###  Units of measurement

Unless otherwise stated units should follow the SI standard.  If other units are used, they should be made as clear as possible by including them in the naming of parameters or functions.

* Time in seconds
* Velocities in m/s
* From the ODrive convention, rotations are in turns 

## Design Goals

* Simple to add new scan patterns
* Smooth and low acceleration transitions within and between different scan patterns
* Ability to run a scan pattern for extended periods,  the patterns should run indefinitely.
* Simple to understand and modular.
* Easy to test components both at the module and system level

### Scan patterns

The following scan patterns are immediately required. One of the goals of all the scan patterns 

#### 'Flower' pattern, from continuously changing the motor speeds.

This rotates the two wedges continuously, modulating their relative speeds.

#### Spiral scan pattern

Move the laser in a well defined set of interleaved spirals to give an optimal trade-off between temporal and spatial sampling.  The goal os this is to build images as fast as possible with even sample point density.

#### Circle pattern

Run the scanner at a fixed radius at a constant speed.  This allows a reading to be taken near a point without causing issues with speckle.

#### Custom patternst

In the future, these may be added by calling out to external code, possibly written in python.


## Architecture

This section describes the general architecture of the scan controller.

### General pattern

Each piece of the core functionality for this system is put into individual sets of classes.  A base class defines the interface that the rest of the code uses to interact with it, and an implementation class where the actual work is done.  This provides several benefits, one is it allows easier simulation and testing by substituting key components in the test environment. It also allows alternate implementations to be developed and tested without touching the previous working code.


```mermaid
classDiagram
	class Interface {		
		<<interface>>		
		+ function()
	}
	Interface <|-- Implementation
	Implementation : function()
	Implementation : int data

```

### Main Components

Following are the main components of the system

```mermaid
classDiagram
	class ScanControllerBase {
		<<interface>>
		+setScanRadius(radius)
		+setScanPattern(patternId)
	}
	class ScanMotionController {
		+OpticalKinematics model
		+ScanPatternFactory area_scan
		+ScanPatternFactory point_scan
		+MotionSegment trajectory
		+EventTiming timer
		+setScanRadius(radius)
		+setScanPattern(patternId)
	}
	class OpticalKinematics {
		<<interface>>
		+prism2beamAngle(Vector2f prismAngles) Vector2f beamAngles
	}
	class ServoSet {
		<<interface>>
		+ move(TrajectoryPoint prismAngles)
	}
	class ScanPatternFactory {
		<<interface>>
		+ trajectory(scan_radius) MotionSegment trajectory
	}
	class MotionSegment {
		<<interface>>
		+at(time) TrajectoryPoint
	}
	ScanControllerBase <|-- ScanMotionController
	ScanMotionController --* ScanPatternFactory : Area
	ScanMotionController --* ScanPatternFactory : Point
	ScanMotionController --o ServoSet
	ScanMotionController --* MotionSegment
	ScanMotionController --o OpticalKinematics
	ScanPatternFactory --> MotionSegment : Generates

```

#### ScanControllerBase

* All normal interactions with the scanner subsystem go via this interface

#### ScanController

* This implements ScanControllerBase
* Executes the motion commands generated from the 'MotionSegment' and issues commands to the servo set.
* Processes requests from the rest of the system to generate an appropriate trajectory for the operating mode.  Typically this is based on scan mode, and zoom level.
* Manages transitions between different trajectories.

#### OpticalKinematics

* Models the optical path between the wedges angles and beam direction
* Provides forward and backwards transforms between the two coordinate spaces.


#### ServoSet

* Provides an abstraction to the code that drives the servos.

#### MotionSegement

* Provides a mapping between a time and a set of motor coordinates
* There are several types of MotionSegments, including one that allows composition into longer trajectories

#### ScanPatternFactory

* Generates a scan pattern consisting of MotionSegments


### MotionSegments

Following is an outline of the motion segment structure.


```mermaid
classDiagram
	class MotionSegment {
		<<interface>>
		+at(time) TrajectoryPoint
	}
	class MotionSegmentChain {
		+at(time) TrajectoryPoint
		+Vector segments 
	}
	class MotionSegmentLinear {
		+at(time) TrajectoryPoint
		real velocity[2]
	}
	MotionSegment <|-- MotionSegmentStop
	MotionSegment <|-- MotionSegmentChain
	MotionSegment <|-- MotionSegmentSpline
	MotionSegment <|-- MotionSegmentLinear
	MotionSegment <|-- MotionSegmentSynchronusZoom
```

### ScanPatternFactory

```mermaid
classDiagram
	class ScanPatternFactory {
		<<interface>>
		+ trajectory(scan_radius) MotionSegment trajectory
	}
	ScanPatternFactory <|-- ScanPatternFactorySpiral
	ScanPatternFactory <|-- ScanPatternFactoryCircle
	ScanPatternFactory <|-- ScanPatternFactoryFlower
```

### ServoSet

```mermaid
classDiagram
	class ServoSet {
		+move(TrajectoryPoint point)
	}
	ServoSet <|-- ServoSetSim
	ServoSet <|-- ODriveServoCAN
	ServoSet <|-- ODriveServoUSB
```