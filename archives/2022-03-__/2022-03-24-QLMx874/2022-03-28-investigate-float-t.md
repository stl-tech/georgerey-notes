# 2022-03-28 Investigation into the source of `float t`



### Executive summary

There is more than one problem with the current code base. 

The current code is prone to create inaccurate computations in both spatial and temporal domain. 

There exists an avenue for the system to freeze abruptly at random times, which would be next to impossible to reproduce reliably.

Here are the recommendations:

* Convert all instances of time treatment to integer-backed, preferably based on `uint64_t` 
* Convert all instances of motor position treatmet to wrapping arithmetic, prefferably based on `uint16_t` integers





========================================================

### Starting point: Eigen:Vector2f at(float t)

`MotionSegmentImpl.hh:58` :  Eigen:Vector2f at(float t)

 * It uses floats for position, and float for t -- both not good.
 * used 13 times
   * in `MotionSegmentImpl.hh:134` `TrajectoryPointT::trajectoryPoint(float t)`
   * `MotionSegment.cc:69`  `TrajectoryPointT MotionSegment::trajectoryPoint(float t) `  ( 3 usages)
   * `MotionSegmentImpl.cc:36` `Eigen::Vector2f MotionSegmentChain::at(float t) ` 

========================================================

Usages of `MotionSegment.cc:63`  |  `TrajectoryPointT MotionSegment::trajectoryPoint(float t) const`

* used 32 times

* Most relevant one::  `ScanControllerMotion.cc:137` |  `bool ScanControllerMotion::updateMotion()`

  * This uses `ScanControllerMotion.hh:72` `float m_currentTime = 0.0` 

  * Critically, this starts from 0.0 at lines 113, 160 and 170; and then is incremented in `ScanControllerMotion:65` | `TrajectoryPointT ScanControllerMotion::nextPoint(float delta)` as `m_currentTime += delta`; 

    * `float delta` comes from `TrajectoryPointT ScanControllerMotion::nextPoint(float delta)`

      * This is declared in an abstract class `ScanControllerBase.hh:74` | ScanControllerBase
      * 3 usages
      * Most relevant ones are in `doScanPattern.cc` and `ScanControllerMotion_test.cc` which are both test classes; can be removed quite easily

    * `float delta` comes from `ScanControllerMotion.cc:216` | `void ScanControllerMotion::doFieldScan()`

      * as `nextPoint(m_updateTiming.stepDelay())`  
        * memo: bad form here; do not use nested calls like this, it hinders debugging
        * memo: the method `.stepDelay()` is called twice, which again is a bad form susceptible to side effects occuring twice
        * memo: implicit demotion from `double` to `float` here; this should throw a warning; but, it doesn't, possibly because the usage is inlined.
        * `m_updateTiming` is `EventDelayTiming m_updateTiming`
          * !! The same name is defined at `ScanController.hh:101` and `ScanControllerMotion.hh:77` but not at `ScanControllerBase` ,  which means that it is not neccesarily clear which instance is being called
        * in `EventDelayTiming.hh:76`

       ```c++
      [[nodiscard]] double stepDelay() const
      { return m_d_time_step_s.count(); }
       ```

      

      * `m_d_time_step_s` is  `DurationT m_d_time_step_s{};`

      * `DurationT` is `typedef std::chrono::duration< double > DurationT;`



=============================================

Worrysome line is at `EventDelayTiming.cc::35`  as this will pick up a "large value" into `m_tp_begin`  which is backed by  `float` ; `m_tp_last` is equally guilty

* 10 usages for `m_tp_begin`

  * `double EventDelayTiming::dTimeElapsed_us()` is never used.
* `double EventDelayTiming::dTimeElapsed()` is used
  
  * <todo> find usages
  * !! `EventDelayTiming.cc:92 | `  `bool EventDelayTiming::DelayEvent()` `m_tp_last` is incremented by a `float` backed value, which is bad ====**BUG REPORT**====
  * 

* 11 usages for `m_tp_last`

  * 7 writes

  * 4 reads

    * most relevant in `EventDelayTiming.cc:101 | `  `bool EventDelayTiming::DelayEvent()` as `std::this_thread::sleep_until(m_tp_last);` 

      * ! This will cause jerky delay.
      * `m_tp_last` suffers from `large value with small increments, in float` problem
      * This needs to be fixed.

      



**Conclusion:**

* @Charles is correct to say that `trajectoryPoint(float t)`  only uses incremental time steps from zero and not a high-valued time from system clock; hence, it should not be affected much by the `large value in float t`  problem; Yet, we observe that the 1st and 2nd (numerical) derivatives are jerky, meaning that the precision is clearly lost somewhere on the way. The second suspect for this loss of precision could be due computing the differences in position itself (when that goes to large value) , not merely due to loss of precision on in time;
* Changing the `DurationT` to be backed by int64_t should be easy
* Propagating the change through the ScanControllerMotion will also change classes deriving from `ScanControllerBase` , so there might be a lot of code changed; I was worried by this,  but I now think that this might be OK, as any place that requires a demotion from `DurationT` to a numeric value can be found and treated easily.
* Since the `MotionSegment` never uses large values of `float t`, the numerical imprecision observed could be coming from somewhere else. The primary suspect is `float motor_position` that never wraps.
* We could make the `float motor_position` to wrap and review the numerical derivatives to compute correctly for wrapped values. However, it is again more correct to go for `uint16_t` based position which wraps naturally (hardware feature) (as per C++11 standard) and provides constant precision. 
  * See: http://open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3337.pdf Section 3.9.1.4 : 

 ```markdown
Unsigned integers, declared unsigned, shall obey the laws of arithmetic modulo 2n where n is the number
of bits in the value representation of that particular size of integer.
 ```

* The statement `m_tp_last += m_d_time_step_s; ` in `EventDelayTiming.cc:92` is bad : This is adding `float small number` to a `float large number` which is bound to be incrementally bad for precision; it will produce jerky delays and depending on the value of `m_tp_last` it may result in the system's clock to be stuck and become unable to be incremented.





====





Frequency content of acceleration, before changing the `SteadyTimePointT` in `EventDelayTiming.cc:28`



![](acceleration-content-before-time-patch.png)



Acceleration value plot -- long-run code for spiral, before the time-patch:



![acceleration-values-before-time-patch](/home/mib07150/git/STL/georgerey-notes/2022-03-24-QLMx874/acceleration-values-before-time-patch.png)





