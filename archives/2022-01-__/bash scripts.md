```bash
watch -n 360 "git pull && (git ls-files --modified --others --exclude-standard | grep . > /dev/null) && { git add . ; git commit -m 'auto' ; git push; }"
```





```bash
startup_message off
defscrollback 99999
# escape ^za
# exec echo "SCREEN session -- note that the escape character is ^z"
hardstaus on
hardstatus firstline
hardstatus string "tp8+screen %S | %= | %{= BW}escape char is ctrl-a%{= dd}"
```





```bash
echo "gap    -> git add, commit and push"
alias gap="git add . && git commit -m 'auto' && git push"
```



```bash
echo "start_notesync -> synchronize daily-notes"
function start_notesync()
{
echo "starting notesync in SCREEN"
pushd /mnt/c/Users/George/Documents/git/STL/daily-notes > /dev/null
screen -S notesync -d -m bash -i -c "__notesync" 
popd > /dev/null
sl
}

function __notesync(){
cd /mnt/c/Users/George/Documents/git/STL/daily-notes
watch -n 360 "git pull && (git ls-files --modified --others --exclude-standard | grep . > /dev/null) && { git add . ; git commit -m>
}
```



---
```bash
watch -n 360 "git pull && (git ls-files --modified --others --exclude-standard | grep . > /dev/null) && { git add . ; git commit -m 'auto' ; git push; }"
```

```bash
echo "dfh     -> show device free space"
alias dfh="df -h | grep -v 100% | grep -v tmpfs | grep -v udev | grep -v sdb1"

```