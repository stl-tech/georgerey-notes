--

When trying to replicate the bug with "equivalent" code -- effect not observed;

see `Bug901_test.cc`





==

At t=5.00 seconds,

```
auto post_change_pt = m_transitionTarget->trajectoryPoint(DurationT::zero())
```

with 

`m_transitionTarget` being the `flower` and NOT "SynchronousZoom" 

we get 

position = (0, -3.08389)

and a compare event

```
compare event
A: +0.00000, +1.00002, +0.01907 | +0.91611, +1.05977, -0.04768
B: +0.00000, +1.00000, +0.00000 | -3.08390, +1.05973, +0.00000
```



which is alarming, 

but:

* This is actually a small distance (0.91610527 − −3.08389) = 3.99999527 ; mod(%) = 999.99527e-3 hence the "transition is successfull" with "intPart=4"

* Why is the AreaScan type = "flower" and not SynchronousZoom?
* Why is the "motion type" set to "-" as if it was disabled?



Actions:

-> verify that this "flower" type reliably gives the same position (0,-3.08) under automated test condition



---



```C++
{ // test at long time and high speed
    Eigen::Vector2f speed = Eigen::Vector2f(10.0f, 20.0f);
    Eigen::Vector2f offset = Eigen::Vector2f(-50.0f, -120.0f);
    auto segment = MotionSegmentLinear(speed, offset);

    auto position_pre = segment.at(one_year - typical_delta);
    auto position_current = segment.at(one_year);
    auto position_post = segment.at(one_year + typical_delta);
    std::array<Eigen::Vector2f,3> position_list = {position_pre, position_current, position_post};

    std::cout << std::setprecision(4) << std::fixed;
    std::cout << "position_pre: " << position_pre.transpose() << std::endl;
    std::cout << "position_current: " << position_current.transpose() << std::endl;
    std::cout << "position_post: " << position_post.transpose() << std::endl;

    auto trajectoryPoint = segment.computeTrajectoryPoint(position_list, typical_delta);
    std::cout << "trajectoryPoint: " << std::endl << "M0         | M1   " << std::endl;
    std::cout << trajectoryPoint.transpose() << std::endl;

    EXPECT_NEAR(trajectoryPoint(0,0), 10.0f, 1e-5f);
    EXPECT_NEAR(trajectoryPoint(1,0), 20.0f, 1e-5f);
    EXPECT_NEAR(trajectoryPoint(0,1), 10.0f, 1e-4f);
    EXPECT_NEAR(trajectoryPoint(1,1), 20.0f, 1e-4f);
    EXPECT_NEAR(trajectoryPoint(0,2), 0.0f, 1e-3f); // reduced precision is expected.
    EXPECT_NEAR(trajectoryPoint(1,2), 0.0f, 1e-3f); // reduced precision is expected.
  }
```

```C
    if (positionDiff <= -1.0f || positionDiff >= 1.0f)
    {
      SPDLOG_ERROR("MotionSegment::computeTrajectoryPoint: double_position_diff too large after wrapping; possible implementation error");
      assert(false); // crash.
    }
```

