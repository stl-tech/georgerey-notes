# Charter

For all MotionSegments

- demonstrate running after a T+1 year
- Demonstrate joining with the spline after T+1 year



* 
* Prepare a report pointing out what has been tested and how

1 day, 1 month, 1 year, 10 years



Test the transition generation



Make a test that just does fuzzing of motion interpolation -- scan pattern transition

---



# Findings

* `MotionSegmentStop` has no example json; it is only ever created from constructor, but not from json
* Transitioning in and out of  a `circle` is unstable, -- does not work
* Transitioning between two SynchronousZooms works OK but can still overload the acceleration in 5 second transition -- consider checking the max acceleration in advance and making the transition shorter or longer as needed
* SynchronousZoom with a s2 period that is too short, will overload the acceleration by creating too large of a spike. Consider improving it to build smooth transition -- or documenting the minimum s2 period, or in fact abandonning it
* There is no factory for motion type "Stop"



!! The TransitionTarget -> duration is zero AND continous is false for circle (=)





Analysis of the ScanControllerMotion Algorithm:

```mermaid
graph TD
	A0 --> Q2
	Q2 --> |float time workaround| A3
	Q2 --> |normal operation| Q4
	A3 --> Q4
	Q4 --> |not complete| Q5
	Q6 --> |something new| Q7a
	
	Q4 --> |complete| Q7a
    Q5 --> |queue empty| A40
    Q5 --> |queue not empty| Q6
    Q6 --> |just a transition| A40	
    

    
	Q7b --> |yes| Q8
	Q7b --> |no| Q14	
	
	Q8  --> |yes| A9A
	A9A --> A9B
	A9B --> A10a	
	A11 --> |continue with error|A9A	
	Q14 --> |take from queue| Anull0
	Q14 --> |queue empty| Q15
	
	Anull0 --> A18
	Q15 --> |yes| A16
	Q15 --> |no| A17
	A16 --> A18
	A17 --> A18
	A18 --> |yes| A20
	A18 --> |no| A21
	A20 --> A10b
	A21 --> A10c
	
	Q8 --> |no| A11
	

	A0(A0<br/>updateMotion<br/>start mutex)
	
	subgraph float time problems?
		Q2{Q2<br/>max time exceeded?}
		A3(A3<br/>motionQueue:=currentMotion)
	end
	
	
	subgraph no change?
		Q4{Q4<br/>motion complete by it's time?}
		Q5{Q5<br/>is there something in the queue}
		Q6{Q6<br/>queue IS transitionTarget}
		A40(A40<br/>motion not complete or motion is transition<br/>return false<br/>)		
	end
	
	Q7a{Q7 > > ><br/>change is needed.}
	Q7b{> > > Q7: change needed.<br/>queue IS transitionTarget?}
	
	subgraph which motion is next?
		Q14{Q14<br/>nothing in queue?}
		Q15{Q15<br/>is current continous?}				
	end
	
	subgraph transition in progress - did it succeed?
		Q8{Q8<br/>is TransitionTarget close?}
	end

	
	A11[A11<br/>transition fail<br/>panic]	

	subgraph set queue
        A16[A16 repeat last motion:<br/>queue:=current]
        A17[A17 halt motion:<br/>queue:=stop]
        Anull0[null op<br/> queue:=queue]
        A9A(A9 transition success<br/>queue:=null<br/>)
	end
	
	subgraph set current and transition
		A18{A18<br/>is the start of<br/>the motion from queue close?}		
		subgraph set current
		    A20[A20<br/>currentMotion:=Queue<br/>transitionTarget:=null<br/>queue:=null]        
        	A21[A21<br/>currentMotion:=CreateTransition<br/>transitionTarget:=queue<br/>queue:=queue]                        
        	A9B(A9 transition success<br/>currentMotion:=transitionTarget<br/>transitionTarget:=null<br/>queue:=null<br/)
		end
        A10a(A10a<br/>new motion started via transition<br/>return true)
        A10b(A10b<br/>new motion plain startedinsta<br/>return true)
		A10c(A10c<br/>transition started<br/>return true)
	end
	
	classDef error fill:#F77
	classDef exit fill:#7F7
	class A11 error

	class A10a exit
	class A10b exit
	class A10c exit
	class A40 exit
	
	class A22 exit
	class A0 exit
	
```







Change 1:

After `Q8N` -> `A11 transition fail`, go to `A9 transition success` instead of `Q14 which motion is next?`

===============



```

const std::ldiv_t s2phase_div = std::div(duration.count(), m_s2period.count());
const float m_fraction_1 = float(s2phase_div.rem)/float(m_s2period.count());
const float s2phaseInPeriodWrapped  = m_fraction_1<0.0f?m_fraction_1+1.0f:m_fraction_1;
auto sineOfPeriod = float(sin(s2phaseInPeriodWrapped * DataToolsN::ftau));
```
